package br.com.contaazul.model;

import br.com.contaazul.comandos.Comando;
import br.com.contaazul.comandos.ProcessaComandos;

public class Robo {

	public Robo(Terreno terreno, Orientacao orientacao, Coordenada coordenada) {
		this.coordenada = coordenada;
		this.terreno = terreno;
		this.orientacao = orientacao;
	}

	public void Executar(String comandos) throws Exception {
		for (Comando comando : new ProcessaComandos(comandos).obterListaDeComandosProcessados()) {
			comando.executar(this);
		}
	}

	public void virarParaAEsquerda() {
		orientacao = orientacao.obterPontosVizinhos(orientacao, Direcao.ESQUERDA);
	}

	public void virarParaADireita() {
		orientacao = orientacao.obterPontosVizinhos(orientacao, Direcao.DIREITA);
	}

	public void mover() {
		switch (orientacao) {
			case NORTE:
				coordenada = coordenada.obtemCoordenadasAtualizadas(coordenada.getX(), coordenada.getY() + 1);
				break;
			case LESTE:
				coordenada = coordenada.obtemCoordenadasAtualizadas(coordenada.getX() + 1, coordenada.getY());
				break;
			case OESTE:
				coordenada = coordenada.obtemCoordenadasAtualizadas(coordenada.getX() - 1, coordenada.getY());
				break;
			case SUL:
				coordenada = coordenada.obtemCoordenadasAtualizadas(coordenada.getX(), coordenada.getY() - 1);
				break;
			default:
				break;
		}
		
		terreno.excedeTamanhoDoTerreno(coordenada.getX(), coordenada.getY());
	}

	public String getPosicaoAtual() {
		return "(" + coordenada.toString() + "," + orientacao.getAbreviacao() + ")";
	}

	private Coordenada coordenada;
	private Orientacao orientacao;
	private final Terreno terreno;
}
