package br.com.contaazul.model;

public enum Orientacao {

	NORTE("N"), SUL("S"), LESTE("E"), OESTE("W");

	Orientacao(String abreviacao) {
		this.abreviacao = abreviacao;
	}

	public Orientacao obterPontosVizinhos(Orientacao orientacao, Direcao direcao) {
		if (orientacao == Orientacao.NORTE || orientacao == Orientacao.SUL)
			return pontosVizinhosNorteESulNaDirecao(direcao);
		else
			return pontosVizinhosLesteEOesteNaDirecao(direcao);
	}

	private Orientacao pontosVizinhosNorteESulNaDirecao(Direcao direcao) {
		return direcao == Direcao.DIREITA ? Orientacao.LESTE : Orientacao.OESTE;
	}

	private Orientacao pontosVizinhosLesteEOesteNaDirecao(Direcao direcao) {
		return direcao == Direcao.DIREITA ? Orientacao.SUL : Orientacao.NORTE;
	}

	public String getAbreviacao() {
		return this.abreviacao;
	}

	private final String abreviacao;
}
