package br.com.contaazul.model;

import br.com.contaazul.utils.RoboForaDosLimitesException;

public class Terreno {

	public Terreno(int eixoX, int eixoY) {
		this.eixoX = eixoX;
		this.eixoY = eixoY;
	}
	
	public int getX() { return this.eixoX; }
	
	public int getY() { return this.eixoY; }
	
	public void excedeTamanhoDoTerreno(int x, int y) throws RoboForaDosLimitesException{
		if(x > eixoX || y > eixoY) {
			throw new RoboForaDosLimitesException("Ultrapassou a extensão permitida do território.");
		}
	}
	
	private int eixoX;
	private int eixoY;
}
