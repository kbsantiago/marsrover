package br.com.contaazul.model;

public class Coordenada {

	public Coordenada(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() { return this.x; }
	public int getY() { return this.y; }
	
	
	public Coordenada obtemCoordenadasAtualizadas(int x, int y) {	
		return new Coordenada(x, y);
	}
	
	@Override
	public String toString() {
		StringBuilder coordenadas = new StringBuilder();
		return coordenadas.append(x).append(",").append(y).toString();
	}
	
	private final int x;
	private final int y;
}
