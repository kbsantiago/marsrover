package br.com.contaazul.utils;

public class RoboForaDosLimitesException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RoboForaDosLimitesException(String message) {
		super(message);
	}
	
}
