package br.com.contaazul.comandos;

import br.com.contaazul.model.Robo;

public class ComandoViraParaEsquerda implements Comando{

	@Override
	public void executar(Robo robo) {
		robo.virarParaAEsquerda();
	}

}
