package br.com.contaazul.comandos;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.contaazul.utils.ComandoInvalidoException;

public class ProcessaComandos {

	public ProcessaComandos(String comandos) {
        this.comandos = comandos;
    }

    public List<Comando> obterListaDeComandosProcessados() throws Exception {
    	if(validarComandos()) {
    		for (String comando : comandos.split("")) {
    			comandosProcessados.add(listaDeComandos.get(comando.toUpperCase()));
        	}
        }
    	
		return comandosProcessados;
    }
    
    private boolean validarComandos() throws Exception {
        for(String comando : comandos.split("")) {
            if(!listaDeComandos.containsKey(comando.toUpperCase())) {
                throw new ComandoInvalidoException("Comandos inválidos.");
            }
        }
        
        return true;
    }

    private final Map<String, Comando> listaDeComandos = new HashMap<String, Comando>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
            put("L", new ComandoViraParaEsquerda());
            put("R", new ComandoViraParaDireita());
            put("M", new ComandoMover());
        }
    };

    private final List<Comando> comandosProcessados = new ArrayList<>();

    private final String comandos;
}
