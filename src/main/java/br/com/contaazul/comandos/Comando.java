package br.com.contaazul.comandos;

import br.com.contaazul.model.Robo;

public interface Comando {
	public void executar(Robo robo);
}
