package br.com.contaazul.comandos;

import br.com.contaazul.model.Robo;

public class ComandoViraParaDireita implements Comando{

	@Override
	public void executar(Robo robo) {
		robo.virarParaADireita();
	}
}
