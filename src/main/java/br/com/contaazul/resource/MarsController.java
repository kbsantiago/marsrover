package br.com.contaazul.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.contaazul.model.Coordenada;
import br.com.contaazul.model.Orientacao;
import br.com.contaazul.model.Robo;
import br.com.contaazul.model.Terreno;

@RestController
@RequestMapping(value = "/rest")
public class MarsController {
	
	@RequestMapping(value = "/mars/{comandos}", method = RequestMethod.POST)
	public ResponseEntity<String> obterCoordenadaAtualAposExecutarComandos(@PathVariable String comandos) {
		
		Robo robo = new Robo(new Terreno(5,5), Orientacao.NORTE, new Coordenada(0,0));
		
		try {
			robo.Executar(comandos);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}
		
		return ResponseEntity.ok(robo.getPosicaoAtual());
	}

}
