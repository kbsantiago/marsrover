package br.com.contaazul;

import org.junit.Assert;
import org.junit.Test;

import br.com.contaazul.model.Coordenada;
import br.com.contaazul.model.Orientacao;
import br.com.contaazul.model.Robo;
import br.com.contaazul.model.Terreno;
import br.com.contaazul.utils.ComandoInvalidoException;
import br.com.contaazul.utils.RoboForaDosLimitesException;

public class RoboTest {

	Terreno terreno = new Terreno(5,5);
	Coordenada coordenada = new Coordenada(0,0);
	Robo robo = new Robo(terreno, Orientacao.NORTE, coordenada);
	
	@Test
	public void testExecutarComandosMMRMMRMMDeveRetornarZeroDoisSul() throws Exception {
		String comandos = "MMRMMRMM";
		
		robo.Executar(comandos);
		
		Assert.assertEquals("(2,0,S)", robo.getPosicaoAtual()); 
	}
	
	@Test
	public void testExecutarComandosMMRMMRMMLMMDeveRetornarZeroZeroOeste() throws Exception {
		String comandos = "MMRMMRMMLMM";
		
		robo.Executar(comandos);
		
		Assert.assertEquals("(0,0,W)", robo.getPosicaoAtual()); 
	}
	
	@Test
	public void testExecutarComandosMMLDeveRetornarZeroDoisOeste() throws Exception {
		String comandos = "MML";
		
		robo.Executar(comandos);
		
		Assert.assertEquals("(0,2,W)", robo.getPosicaoAtual()); 
	}
	
	
	@Test(expected=ComandoInvalidoException.class)
	public void testExecutarComandosAAADeveRetornarErro() throws Exception {
		String comandos = "AAA";
		
		robo.Executar(comandos);
	}
	
	
	@Test(expected=RoboForaDosLimitesException.class)
	public void testExecutarComandosMMMMMMMMMMMMMMMMMMMMMMMMDeveRetornarZeroDoisSul() throws Exception {
		String comandos = "MMMMMMMMMMMMMMMMMMMMMMMM";
		
		robo.Executar(comandos);
	}
}
